#user 'west-tech' virtual host 'west-tech.ru' configuration file
<VirtualHost 127.0.0.1:8080>
	ServerName west-tech.ru
	AddDefaultCharset off
	AssignUserID west-tech west-tech
	DocumentRoot /var/www/west-tech/data/www/west-tech.ru
	ServerAdmin kotoverman@gmail.com
	CustomLog /var/www/httpd-logs/west-tech.ru.access.log combined
	ErrorLog /var/www/httpd-logs/west-tech.ru.error.log
	ServerAlias west-tech.infowesttechru.fvds.ru www.west-tech.ru
	DirectoryIndex index.html index.php
	SetEnvIf X-Forwarded-Proto https HTTPS=on
	<FilesMatch "\.ph(p[3-5]?|tml)$">
		SetHandler application/x-httpd-php
	</FilesMatch>
	<FilesMatch "\.phps$">
		SetHandler application/x-httpd-php-source
	</FilesMatch>
	<IfModule php5_module>
		php_admin_value sendmail_path "/usr/sbin/sendmail -t -i -f kotoverman@gmail.com"
		php_admin_value upload_tmp_dir "/var/www/west-tech/data/mod-tmp"
		php_admin_value session.save_path "/var/www/west-tech/data/mod-tmp"
		php_admin_value open_basedir "/var/www/west-tech/data:."
	</IfModule>
	<IfModule php7_module>
		php_admin_value sendmail_path "/usr/sbin/sendmail -t -i -f kotoverman@gmail.com"
		php_admin_value upload_tmp_dir "/var/www/west-tech/data/mod-tmp"
		php_admin_value session.save_path "/var/www/west-tech/data/mod-tmp"
		php_admin_value open_basedir "/var/www/west-tech/data:."
	</IfModule>
</VirtualHost>
<Directory /var/www/west-tech/data/www/west-tech.ru>
	Options +Includes -ExecCGI
	<IfModule php5_module>
		php_admin_flag engine on
	</IfModule>
	<IfModule php7_module>
		php_admin_flag engine on
	</IfModule>
</Directory>
