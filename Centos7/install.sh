yum update -y
yum install -y nano mc stunnel catdoc xpdf munin nagios sphinx
echo export EDITOR="nano" >> ~/.bashrc
echo LANG="ru_RU.UTF-8" >> ~/.bashrc


/* git */
rpm -Uvh http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-1.noarch.rpm
//rpm -Uvh http://opensource.wandisco.com/centos/6/git/x86_64/wandisco-git-release-6-1.noarch.rpm
yum install -y git


/* nginx + php */
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
yum -y install httpd httpd-itk mod_ssl nginx ImageMagick
yum -y install php70w php70w-cli php70w-common php70w-fpm php70w-devel php70w-gd php70w-imap php70w-mbstring php70w-mcrypt php70w-mysql php70w-opcache php70w-pdo php70w-pecl-memcache php70w-pecl-xdebug php70w-pgsql php70w-soap php70w-tidy php70w-xml php70w-pecl-geoip
yum -y install phpmyadmin
systemctl enable nginx.service
systemctl enable php-fpm.service
/* nginx + php */


/* bitrix */
yum install -y php-curl php-fpm
/* bitrix */


/* база данных */
yum -y install mariadb-server mariadb
systemctl enable mariadb.service
systemctl start mariadb
/usr/bin/mysql_secure_installation
#mkdir /var/lib/mysql/tmp
#chown mysql:mysql /var/lib/mysql/tmp
#chmod 775 -R /var/lib/mysql/tmp
#id mysql
#файл fstab добавляем запись uid и gid пользователя mysql
#tmpfs /var/lib/mysql/tmp tmpfs rw,gid=993,uid=995,size=1G,nr_inodes=10k,mode=0700 0 0
nano /usr/lib/systemd/system/mariadb.service
[Service]
LimitNOFILE = 65535
LimitNPROC = 65535
nano /etc/systemd/system/mariadb.service.d/nofile.conf
LimitNOFILE=65000
/* база данных */


/* лимиты */
nano /etc/security/limits.conf
*   soft    nproc   65000
*   hard    nproc   1000000
*   -    nofile  1048576
root - memlock unlimited
/* лимиты */


/* сертификат */
#cd /usr/local/sbin
#sudo wget https://dl.eff.org/certbot-auto
#cd /tmp
#git clone https://github.com/certbot/certbot
#sudo chmod a+x /usr/local/sbin/certbot-auto
#certbot-auto certonly --webroot --agree-tos --email info@site.ru -w /home/bitrix/www/ -d site.ru -d www.site.ru -d test.site.ru
#openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048
#service nginx reload
#
#nano /etc/crontab
#>>добавляем строки
#30 2 * * 1 /usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log
#35 2 * * 1 /etc/init.d/nginx reload
/* сертификат */



/* автообновление */
yum install -y yum-cron
systemctl enable yum-cron.service
systemctl start yum-cron.service
/* автообновление */


/* monit */
yum install -y monit
systemctl enable monit.service
systemctl start monit.service
/* monit */


/* yandex-disk */
rpm -Uvh http://repo.yandex.ru/yandex-disk/yandex-disk-latest.x86_64.rpm
rpm --import http://repo.yandex.ru/yandex-disk/YANDEX-DISK-KEY.GPG
yum install yandex-disk
/* yandex-disk */
