yum -y install nano mc
yum -y update
echo export EDITOR="nano" >> ~/.bashrc
echo LANG="ru_RU.UTF-8" >> ~/.bashrc

rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

// автообновление
yum install -y yum-cron
systemctl enable yum-cron.service
systemctl start yum-cron.service

//monit
yum install -y monit
systemctl enable monit.service
systemctl start monit.service

//git
rpm -Uvh http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-1.noarch.rpm
//rpm -Uvh http://opensource.wandisco.com/centos/6/git/x86_64/wandisco-git-release-6-1.src.rpm
yum install -y git

yum -y install mariadb-server mariadb
systemctl start mariadb

yum -y install httpd httpd-itk mod_ssl nginx ImageMagick
yum -y install php70w php70w-cli php70w-common php70w-fpm php70w-devel php70w-gd php70w-imap php70w-mbstring php70w-mcrypt php70w-mysql php70w-opcache php70w-pdo php70w-pecl-memcache php70w-pecl-xdebug php70w-pgsql php70w-soap php70w-tidy php70w-xml php70w-pecl-geoip
yum -y install phpmyadmin

systemctl enable nginx.service
systemctl enable mariadb.service
systemctl enable php-fpm.service

rpm -Uvh http://repo.yandex.ru/yandex-disk/yandex-disk-latest.x86_64.rpm
rpm --import http://repo.yandex.ru/yandex-disk/YANDEX-DISK-KEY.GPG
yum install yandex-disk
